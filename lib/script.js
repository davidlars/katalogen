
var idx = null;

//var articles_collection = document.getElementById("programvaror").getElementsByTagName('li');
var articles_collection = document.getElementById("programvaror").getElementsByClassName('item');
var articles_array = Array.prototype.slice.call(articles_collection);
var mapArticles = new Map(articles_array.map(i => [i.id, i]));

const searchI = document.getElementById("site-search");

searchI.addEventListener("keypress", (e) => {

  if (e.keyCode === 13) {
    searchInput();
  }

});

searchI.addEventListener('input', (e) => {
  if(e.currentTarget.value===""){
    showAll();
  }else{
    search(e.currentTarget.value);
  }
})

function searchTag(value){
  document.getElementById("site-search").value = value;
  search(value);
}

function searchInput(){
  // inside, we will need to achieve a few things:
  // 1. declare and assign the value of the event's target to a variable AKA whatever is typed in the search bar
  let value = document.getElementById("site-search").value;//e.target.value

  search(value);
}

function search(value){

  // 2. check: if input exists and if input is larger than 0
  if (value && value.trim().length > 0){
    // 3. redefine 'value' to exclude white space and change input to all lowercase
    value = value.trim().toLowerCase()
    // 4. return the results only if the value of the search is included in the person's name
    // we need to write code (a function for filtering through our data to include the search input value)
    var searchResult = idx.search(value+"~1");

    show(searchResult);

    showContent(searchResult);

  } else {
    // 5. return nothing
    // input is invalid -- show an error message or show no results
    showAll();
  }
}

function display(number,max){
  let hits = "Visar " +"<mark>"+number+"</mark> programvaror av " + max;
  document.getElementById("hits").innerHTML = hits;
}

display(articles_array.length,articles_array.length);

function showAll(){

  display(articles_array.length,articles_array.length);

  articles_array.forEach(article => {
    // 👇️ hide element (still takes up space on page)
    article.style.display = 'block';
  });
}

function showContent(result) {
  var temp = document.getElementsByTagName("template")[0];


  var clon = temp.content.cloneNode(true);

  result.forEach(function(item) {
    var hit = mapArticles.get(item.ref);
    clon.appendChild(hit);
  });

  var navigation = document.getElementById("paging");

  document.getElementById("listning").insertBefore(clon,navigation);
  //document.body.appendChild(clon);

}

function show(result){

  display(result.length,articles_array.length);

  articles_array.forEach(article => {
    // 👇️ hide element (still takes up space on page)
    article.style.display = 'none';
  });

  result.forEach(function(item) {
    var hit = mapArticles.get(item.ref);
    hit.style.display = 'block';
  });


}

const loadScript = (FILE_URL, async = true, type = "text/javascript") => {
  return new Promise((resolve, reject) => {
    try {
      const scriptEle = document.createElement("script");
      scriptEle.type = type;
      scriptEle.async = async;
      scriptEle.src =FILE_URL;

      scriptEle.addEventListener("load", (ev) => {
        resolve({ status: true });
      });

      scriptEle.addEventListener("error", (ev) => {
        reject({
          status: false,
          message: `Failed to load the script ＄{FILE_URL}`
        });
      });

      document.body.appendChild(scriptEle);
    } catch (error) {
      reject(error);
    }
  });
};

document.getElementsByTagName("details")[0].addEventListener("toggle", (event) => {
  if (event.target.open && !idx) {
    console.log("Användaren vill söka efter programvaror");
    loadScript("lib/lunr.min.js")
    .then( data  => {
      fetch('index.json', {
        method: 'GET'
      }).then(response => response.json()).then(function(data) {
        idx = lunr.Index.load(data);
        console.log("Laddat index som krävs för sökning");
      }).catch(function(err) {
        console.log(err);
      });
    })
    .catch( err => {
      console.error(err);
    });
  } else {
    /* the element was toggled closed */
  }
});


/**
<script>

DiscourseEmbed = {
discourseUrl: 'https://forum.jobtechdev.se/',
topicId: "338",
className: 'Light',
};

(function() {
var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
})();

(function () {
function postMessageReceived(e) {
if (!e) {
return;
}

if (e.data && e.data.type === "discourse-resize" && e.data.embedId) {
var elem = document.getElementById(e.data.embedId);
if (elem) {
elem.height = e.data.height + "px";
}
}
}
})();
</script>
**/
